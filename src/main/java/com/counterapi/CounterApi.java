package com.counterapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.LongAdder;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Impements an API for keeping track of a set of counters updating their
 * values. The counters are stored in a ConcurrentHashMap in order to maintain
 * synchronizity as multiple threads work towards it. The counters are comprised
 * by sets of String, LongAdder pairs.
 *
 * Root resource (exposed at "counterapi" path)
 *
 */
@Path("/")
@Singleton
public class CounterApi {

    private LongAdder newValue;
    private LongAdder currentValue;

    private final ConcurrentMap<String, LongAdder> counters = new ConcurrentHashMap<>();

    /**
     * Method for incrementing a counter. By sending a HTTP GET request to the
     * specified path, the counter specified in the parameter is incremented and
     * a message is returned telling the user that it's been incremented.
     *
     * @param counterName Name of counter to increment
     * @return Message telling user that the counter has been incremented
     */
    @GET
    @Path("incrementCounter/{counterName}")
    @Produces(MediaType.TEXT_PLAIN)
    public String incrementCounter(@PathParam("counterName") String counterName) {

        currentValue = counters.computeIfAbsent(counterName, k -> new LongAdder());
        if (currentValue.intValue() == 0) {
            currentValue.increment();
            return "Created counter " + counterName + "! Current value is 1.\n";
        }

        currentValue = counters.computeIfPresent(counterName, (k, v) -> v);
        currentValue.increment();
        System.out.println("Counter " + counterName + " has been incremented!"
                + " Current value is " + currentValue);
        return "Incremented counter " + counterName + " by one! Current value is " + currentValue + ".\n";

    }

    /**
     * Method for retrieving the value of a counter. Retrieves the value of a
     * given counter (specified in the parameter) and returns it to the user.
     *
     * @param counterName Name of counter
     * @return Value of counter
     */
    @GET
    @Path("/getValue/{counterName}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getValue(@PathParam("counterName") String counterName) {

        System.out.println("Received a getValue request for counter " + counterName + "!");

        if (counters.get(counterName) == null) {
            System.out.println("Could not find a value for counter " + counterName);
            return "No value found for counter " + counterName + "!";
        } else {
            currentValue = counters.get(counterName);
            System.out.println("Retrieved value " + currentValue + " for counter " + counterName);
            return "Current value of counter " + counterName + " is " + currentValue + ".\n";
        }

    }

    /**
     * Method for getting the values of all counters. Retrieves a HashMap of
     * storing the counters and returns it to the user.
     *
     * @return HashMap of currently stored counters.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @GET
    @Path("/getCounters")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCounters() throws JsonProcessingException {
        
        String json = "";

        System.out.println("Received a getCounters request!");

        if (counters.isEmpty()) {
            System.out.println("No counters available");
        } else {
            System.out.println("Counters: " + counters);
        }
        
        ObjectMapper mapper = new ObjectMapper();
        
        try {
            json = mapper.writeValueAsString(counters);
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            json = mapper.writeValueAsString("Could not get counters");
        }
        
        return json;
    }
}
