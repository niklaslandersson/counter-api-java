package com.counterapi;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class CounterApiTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(CounterApi.class);
    }

    /**
     * Test to see that the incrementCounter method returns message "Incremented
     * counter {counterName} by one! Current value is {currentValue}."
     */
    @Test
    public void testCreateAndIncrementCounter() {
        String responseMsg = target().path("counterapi/incrementCounter/Test").request().get(String.class);
        assertEquals("Created counter Test! Current value is 1.\n", responseMsg);
    }

    /**
     * Test to see that the getValue method returns message "Current value of
     * counter {counterName} is {currentValue}."
     */
    @Test
    public void testGetValue() {
        target().path("counterapi/incrementCounter/Test").request().get(String.class);
        String responseMsg = target().path("counterapi/getValue/Test").request().get(String.class);
        assertEquals("Current value of counter Test is 1.\n", responseMsg);
    }

    /**
     * Test to see that the getCounters method returns message showing status of
     * current counters.
     */
    @Test
    public void testGetCounters() {
        target().path("counterapi/incrementCounter/Test1").request().get(String.class);
        target().path("counterapi/incrementCounter/Test2").request().get(String.class);
        String responseMsg = target().path("counterapi/getCounters").request().get(String.class);
        assertEquals("{\"Test1\":1,\"Test2\":1}", responseMsg);
    }

    /**
     * Tests whether the incrmeentCounter method is thread-safe.
     *
     * @throws InterruptedException
     */
    @Test
    public void testIncrementCounterThreading() throws InterruptedException {
        
        ExecutorService executor = Executors.newFixedThreadPool(3);

        try {
            
            Runnable task = () -> {
                target().path("counterapi/incrementCounter/Test").request().get(String.class);
            };

            IntStream.range(0, 10000)
                    .forEach(i -> executor.submit(task));

            executor.shutdown();
            executor.awaitTermination(60, TimeUnit.SECONDS);

            String responseMsg = target().path("counterapi/getValue/Test").request().get(String.class);
            System.out.println("responseMsg: " + responseMsg);
            assertEquals(responseMsg, "Current value of counter Test is 10000.\n");
            
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
